# CHANGELOG

## Version 0.5.2

- added .gitignore
- changed FALLTHU comment in psfid.c in order to silence the warning
- moved all documentation out of the doc directory
- fixed version number in psftools_version.h

## Version 0.5.1

- fixed memory leak
- fixed potential memory leak
- fixed 2 file handle leaks

## Version 0.5

- added -l option to psfid

## Version 0.4

- added psft tool

## Version 0.3

- upgrades to the demo font and the README
- psfc: better parsing and error detection
- psfd: now works with pipes as input

## Version 0.2

- initial release
